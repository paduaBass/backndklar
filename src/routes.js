const express = require('express');

const UserController = require("./controllers/UserController");
const AgendamentoController = require("./controllers/AgendamentoController");


const routes = express.Router();

routes.post('/new', UserController.store);

routes.post('/agendamento', AgendamentoController.store);

routes.post('/login', UserController.login);

routes.get('/agendamentos:id', AgendamentoController.index);
routes.get('/agendamentos', AgendamentoController.listAgendamentos);


module.exports = routes;