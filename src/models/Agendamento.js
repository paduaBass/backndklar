const mongoose = require('mongoose');

const AgendamentoSchema = new mongoose.Schema({
    data: String,
    hora: String,
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }

});

module.exports = mongoose.model('Agendamento', AgendamentoSchema);