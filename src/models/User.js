const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    nome: String,
    sobrenome: String,
    email: String,
    dataDeNascimento: Date,
    telefone: Number,
    celular: Number,
    cidade: String,
    endereco: String,
    caixaPostal: Number,
    nomeResponsavel: String,
    sobrenomeResponsavel: String,
    telefoneResponsavel: Number,
    emailResponsavel: Number,
    senha: String
});

module.exports = mongoose.model('User', UserSchema);