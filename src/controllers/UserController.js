const User = require('../models/User');

module.exports = {

    async login(req, res) {
        const {email, senha} = req.body;

        let user = await User.findOne({email, senha});

        if(user){
            return res.json(user);
        }else{
            return res.status(401).json({menssage: 'sjekk legitimasjonsbeskrivelsen'});
        }
    },

    async store(req, res) {
        const { 
            nome, 
            sobrenome, 
            email, 
            dataDeNascimento, 
            telefone, 
            celular, 
            cidade,
            endereco,
            caixaPostal,
            nomeResponsavel,
            telefoneResponsavel,
            emailResponsavel,
            sobrenomeResponsavel,
            senha
        } = req.body;
       

        let user = await User.findOne({ email });

        if(!user) {
            user = await User.create({ 
                nome, 
                sobrenome, 
                email, 
                dataDeNascimento, 
                telefone, 
                celular, 
                cidade, 
                endereco, 
                caixaPostal, 
                nomeResponsavel, 
                sobrenomeResponsavel, 
                telefoneResponsavel, 
                emailResponsavel,
                senha
            });
        }

        return res.json(user);
    }
};