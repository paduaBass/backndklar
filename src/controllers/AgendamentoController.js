const Agendamento = require('../models/Agendamento');

module.exports = {

    async store(req, res) {
        const { data, hora } = req.body;
        const { user_id } = req.headers;

        console.log(user_id);

        const agendamento = await Agendamento.create({ user: user_id, data, hora});
        return res.json(agendamento);
    },

    async index(){

        const { user_id } = req.query;



        const agendamento = await Agendamento.find({
           user: user_id
        });

        return res.json(agendamento);
    },

    async listAgendamentos(req, res) {
        const agendamento = await Agendamento.find();

        return res.json(agendamento)

    }



};